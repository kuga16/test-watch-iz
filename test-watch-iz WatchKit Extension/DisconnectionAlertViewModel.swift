import SwiftUI
import UserNotifications

final class DisconnectionAlertViewModel: NSObject, ObservableObject, UNUserNotificationCenterDelegate {
//    @AppStorage("delayTime") var delayTime: Int = 10
//    @AppStorage("normalWay") var normalWayMethod: Int = 0
//    @AppStorage("connectedNotice") var isConnectedNoticeOn: Bool = true
    
    func onNotice(body: String, identifier: String) {
        let center: UNUserNotificationCenter = UNUserNotificationCenter.current()
        let content: UNMutableNotificationContent = UNMutableNotificationContent()
        var trigger: UNTimeIntervalNotificationTrigger!
        var request: UNNotificationRequest!
        
        content.body = NSLocalizedString(body, comment: "")
        // content.sound = UNNotificationSound.default
        trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
        request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)

        center.add(request, withCompletionHandler: nil)
    }
    
    func onAlarm(body: String, identifier: String) {
        let center: UNUserNotificationCenter = UNUserNotificationCenter.current()
        let content: UNMutableNotificationContent = UNMutableNotificationContent()
        let trigger: UNTimeIntervalNotificationTrigger
        let request: UNNotificationRequest
        let category: UNNotificationCategory
        let snoozeAction: UNNotificationAction
        let stopAction: UNNotificationAction
        center.delegate = self
        
        snoozeAction = UNNotificationAction (
            identifier: "startSnooze", title: NSLocalizedString("snooze", comment: ""), options: [.foreground]
        )
        
        stopAction = UNNotificationAction (
            identifier: "stopDisconnectAlarm", title: NSLocalizedString("stop", comment: ""), options: [.destructive, .foreground]
        )
       
        category = UNNotificationCategory (
            identifier: "disconnectAlarmCategory", actions: [snoozeAction, stopAction], intentIdentifiers: [], options: []
        )
        
        center.setNotificationCategories([category])
        content.body = NSLocalizedString(body, comment: "")
        //content.sound = UNNotificationSound.default
        content.categoryIdentifier = "disconnectAlarmCategory"
        trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
        request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)

        center.add(request, withCompletionHandler: nil)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: () -> Void) {
            
            switch response.actionIdentifier {
                case "stopDisconnectAlarm":
                    print("停止")
                case "startSnooze":
                    print("スヌーズ")
                default:
                    print("デフォルト")
                    break
            }
        completionHandler()
    }
    
}
