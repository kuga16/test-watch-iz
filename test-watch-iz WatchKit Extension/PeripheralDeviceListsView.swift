import SwiftUI

struct PeripheralDeviceListsView: View {
    @ObservedObject var mainCentral: MainCentral = MainCentral()
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("接続中のデバイス")) {
                    if self.mainCentral.connectedPeripheral == nil {
                        Text("searching...")
                    } else {
                        HStack {
                            Text(self.mainCentral.getName(peripheral: self.mainCentral.connectedPeripheral!))
                            Spacer()
                            Image(systemName: "checkmark")
                                .foregroundColor(.green)
                        }
                    }
                }
                
                Section(header: Text("その他のデバイス")) {
                    if mainCentral.candidatePeripherals.count == 0 {
                        Text("searching...")
                    }
                    ForEach(0..<self.mainCentral.candidatePeripherals.count, id: \.self) { i in
                        Button(action: {
                            self.mainCentral.connect(peripheral: mainCentral.candidatePeripherals[i])
                        }) {
                            HStack {
                                Text(self.mainCentral.getName(peripheral: mainCentral.candidatePeripherals[i]))
                            }
                        }
                    }
                }
            }
            .navigationTitle(Text("bluetoothSettings"))
        }
        .onAppear() {
            self.mainCentral.startScan()
        }
        .onDisappear() {
            self.mainCentral.stopScan()
        }
    }
}

struct PeripheralDeviceListsView_Previews: PreviewProvider {
    static var previews: some View {
        PeripheralDeviceListsView()
    }
}
