import WatchKit
import CoreBluetooth
import UserNotifications
import AVFoundation

class ExtensionDelegate: NSObject, WKExtensionDelegate, ObservableObject, CBCentralManagerDelegate, CBPeripheralDelegate, AVAudioPlayerDelegate {
    let disconnectionAlertViewModel: DisconnectionAlertViewModel = DisconnectionAlertViewModel()
    let mainCentral: MainCentral = MainCentral()
    var centralManager: CBCentralManager!
    var connectedPeripheral: CBPeripheral?
    var isConnect: Bool = true
    
    override init() {
        super.init()
        self.centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
    func applicationDidFinishLaunching() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            if error != nil {
                return
            }
        }
    }

    func applicationWillResignActive() {
        print("No active 1")
    }
    
    func applicationDidEnterBackground() {
        print("No active 2")
        
        if mainCentral.connectedPeripheralIdentifier.isEmpty {
            return
        }
        
        self.connectedPeripheral = centralManager.retrievePeripherals(withIdentifiers: [UUID(uuidString: mainCentral.connectedPeripheralIdentifier)!])[0]
        self.centralManager.connect(self.connectedPeripheral!, options: nil)
    }
    
    func applicationDidBecomeActive() {
        print("Active")
    }

    func handle(_ backgroundTasks: Set<WKRefreshBackgroundTask>) {
        // Sent when the system needs to launch the application in the background to process tasks. Tasks arrive in a set, so loop through and process each one.
        for task in backgroundTasks {
            // Use a switch statement to check the task type
            switch task {
            case let backgroundTask as WKApplicationRefreshBackgroundTask:
                // Be sure to complete the background task once you’re done.
                backgroundTask.setTaskCompletedWithSnapshot(false)
            case let snapshotTask as WKSnapshotRefreshBackgroundTask:
                // Snapshot tasks have a unique completion call, make sure to set your expiration date
                snapshotTask.setTaskCompleted(restoredDefaultState: true, estimatedSnapshotExpiration: Date.distantFuture, userInfo: nil)
            case let connectivityTask as WKWatchConnectivityRefreshBackgroundTask:
                // Be sure to complete the connectivity task once you’re done.
                connectivityTask.setTaskCompletedWithSnapshot(false)
            case let urlSessionTask as WKURLSessionRefreshBackgroundTask:
                // Be sure to complete the URL session task once you’re done.
                urlSessionTask.setTaskCompletedWithSnapshot(false)
            case let relevantShortcutTask as WKRelevantShortcutRefreshBackgroundTask:
                // Be sure to complete the relevant-shortcut task once you're done.
                relevantShortcutTask.setTaskCompletedWithSnapshot(false)
            case let intentDidRunTask as WKIntentDidRunRefreshBackgroundTask:
                // Be sure to complete the intent-did-run task once you're done.
                intentDidRunTask.setTaskCompletedWithSnapshot(false)
            default:
                // make sure to complete unhandled task types
                task.setTaskCompletedWithSnapshot(false)
            }
        }
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        // Default
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) -> Void {
        print("Connection completed...")
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) -> Void {
        print("Connection failed...")
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) -> Void {
        let dispatchGroup = DispatchGroup()
        let dispatchQueue = DispatchQueue(label: "dispatchQueue")
        let alarmDispatchGroup = DispatchGroup()
        let alarmDispatchQueue = DispatchQueue(label: "alarmDispatchQueue", attributes: .concurrent)
        let normalWayMethod: Int = 1
        
        print("Disconnect...")
        sleep(10)
        
        dispatchQueue.async(group: dispatchGroup) {
            self.centralManager.connect(peripheral, options: nil)
        }
        
        dispatchQueue.async(group: dispatchGroup) {
            switch normalWayMethod {
                case 0:
                    print("notification")
                    
                    // notification
                    self.disconnectionAlertViewModel.onNotice (
                        body: "disconnected",
                        identifier: "disconnectNotice"
                    )
                    
                case 1:
                    print("notification + alarm")
                    
                    // notification
                    alarmDispatchQueue.async(group: alarmDispatchGroup) {
                        self.disconnectionAlertViewModel.onAlarm (
                            body: "disconnected",
                            identifier: "disconnectAlarm"
                        )
                    }
                    
                    // alarm
                    DispatchQueue.main.async {
                        let originPlayers = OrigAudioPlayer(sleepTime: 9, loopCnt: 7, soundName: "Chime")
                        
                        for player in originPlayers.players {
                            if player.isPlaying {
                                print("stop")
                                player.stop()
                            }
                            player.play()
                            self.originSleep(sleepTime: Int(originPlayers.sleepTime))
                        }
                    }
                    
                default:
                break
            }
        }
    }
    
    func originSleep(sleepTime: Int) {
        let endDate: Date = Calendar.current.date(byAdding: .second, value: sleepTime, to: Date())!
        
        print("Loop start")
        repeat {
        } while Date() <= endDate
    }

}
