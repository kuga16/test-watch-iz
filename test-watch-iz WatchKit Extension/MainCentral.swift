import SwiftUI
import CoreBluetooth
import UserNotifications

class MainCentral: NSObject, ObservableObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    var centralManager: CBCentralManager!
    @Published var connectedPeripheral: CBPeripheral?
    @Published var candidatePeripherals: [CBPeripheral] = []
    @AppStorage("connectedPeripheralIdentifier") var connectedPeripheralIdentifier: String = ""
    
    override init() {
        super.init()
        centralManager = CBCentralManager(delegate: self, queue: nil)
        //centralManager.delegate = self
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        // Default
    }
    
    func startScan() -> Void {
        print("Start scan")
        candidatePeripherals.removeAll()
        centralManager.scanForPeripherals(withServices: nil, options: nil)
    }
    
    func stopScan() -> Void {
        print("Stop scan")
        centralManager.stopScan()
    }
    
    // スキャンを開始後、見つけたPeripheralを格納
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) -> Void {
        
        if peripheral.name == nil {
            return
        }
        
        if connectedPeripheralIdentifier.isEmpty && !candidatePeripherals.contains(peripheral) {
            self.candidatePeripherals.append(peripheral)
            return
        }
        
        self.connectedPeripheral = centralManager.retrievePeripherals(withIdentifiers: [UUID(uuidString:  connectedPeripheralIdentifier)!])[0]
    }
    
    // 接続処理
    func connect(peripheral: CBPeripheral) -> Void {
        self.connectedPeripheral = peripheral
        self.connectedPeripheralIdentifier = peripheral.identifier.uuidString
        centralManager.connect(peripheral, options: nil)
    }
    
    // 接続成功時
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) -> Void {
        print("Connection completed...")
        stopScan()
       
        // 他のデバイス配列から接続したデバイスを削除
        if let i = self.candidatePeripherals.firstIndex(of: peripheral) {
            self.candidatePeripherals.remove(at: i)
        }
    }

    // 接続失敗時
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) -> Void {
        //
    }
    
    // 切断時の処理
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) -> Void {
        //
    }
    
    func getName(peripheral: CBPeripheral) -> String {
        peripheral.name!
    }
    
    func getState(peripheral: CBPeripheral) -> Int {
        peripheral.state.rawValue
    }
}
