import WatchKit
import AVFoundation

class OrigAudioPlayer: NSObject {
    var sleepTime: Int32 = 0
    var loopCnt: Int32 = 0
    var soundName: String = ""
    var players: [AVAudioPlayer] = []
    
    init(sleepTime: Int32, loopCnt: Int32, soundName: String) {
        self.sleepTime = sleepTime
        self.loopCnt = loopCnt
        self.soundName = soundName
        self.players = OrigAudioPlayer.getPlayers(name: soundName, loopCnt: loopCnt)
    }
    
    static func getAudioArray(soundName: String) -> [AVAudioPlayer] {
        let data: AVAudioPlayer
        var dataName: String
        
        switch soundName {
        case "AlarmClock":
            dataName = "AlarmClock-Alarm1"
            
        case "Beep":
            dataName = "Beep-Alarm1"

        case "Buzzer":
            dataName = "Buzzer"

        case "Chime":
            dataName = "Chime"
            
        case "Fanfare":
            dataName = "Fanfare"
            
        case "Siren":
            dataName = "Siren-Alarm1"
            
        default:
            dataName = ""
        }
        
        do {
            data = try AVAudioPlayer(data: NSDataAsset(name: dataName)!.data)
            data.prepareToPlay()
            data.volume = 0.01
            return [data]
        } catch {
            print("getAudioArrayError: \(error.localizedDescription)")
            return []
        }
        
    }
    
    static func getPlayers(name: String, loopCnt: Int32) -> [AVAudioPlayer] {
        var array: [AVAudioPlayer] = []
        
        for _ in 1...loopCnt {
            array += OrigAudioPlayer.getAudioArray(soundName: name)
        }
        return array
    }
    
}
